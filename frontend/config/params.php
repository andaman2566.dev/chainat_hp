<?php
return [
    'adminEmail' => 'admin@example.com',
    'messageURL' => YII_ENV_DEV ? 'http://localhost:3000/api/add-message' : 'http://q.chainathospital.org/node/api/add-message' 
];
