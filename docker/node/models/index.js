const TbService = require('./tb_service')
const TbServiceGroup = require('./tb_service_group')
const TbCounterService = require('./tb_counter_service')
const TbDrugConfig = require('./tb_drug_config')
const TbServiceTsSlot = require('./tb_service_tslot')
const TbQueue = require('./tb_queue')
const TbQueueTrans = require('./tb_qtrans')
const TbServiceStatus = require('./tb_service_status')
const TbCaller = require('./tb_caller')
const TbLastQueue = require('./tb_last_queue')
const TbServiceProfile = require('./tb_service_profile')
const User = require('./user')
const TbCallingConfig = require('./tb_calling_config')
const TbSound = require('./tb_sound')
const SettingModel = require('./setting')
const TbServiceType = require('./tb_service_type')
const TbDisplay = require('./tb_display')
const TbCounterServiceType = require('./tb_counterservice_type')
const TbTicket = require('./tb_ticket')
const TbSoundStation = require('./tb_sound_station')

module.exports = {
  TbService,
  TbServiceGroup,
  TbCounterService,
  TbDrugConfig,
  TbServiceTsSlot,
  TbQueue,
  TbQueueTrans,
  TbServiceStatus,
  TbCaller,
  TbLastQueue,
  TbServiceProfile,
  User,
  TbCallingConfig,
  TbSound,
  SettingModel,
  TbServiceType,
  TbDisplay,
  TbCounterServiceType,
  TbTicket,
  TbSoundStation
}